
$("document").ready(function(){

  //jquery slide booking-form

  $(".booking1").click(function(){
    $("#booking").slideToggle();
      
  });
  $(".hotel .sub-title a").click(function(){
    var li=$(this).parent();
    if (li.hasClass ('selected'))
      return true;
    else {
      $(".hotel div").removeClass('selected');
      li.addClass('selected');
    }
    return false;
  });
  // $(".sub-title").click(function (){
  //     $(".submenu1").slideToggle();
      
  // });

  });
  
  // jquery datepicker
  
  $(function () {  
  $(".datepicker").datepicker({       
    autoclose: true,         
    todayHighlight: true 
  });
  });

  //jquery change seclect box

$('document').ready(function() {
  $("#room_type").change(function() {
    $("input[type='checkbox']").prop('checked',false); //reset checkbox no checked
    var id=$("#room_type").val();
    var num_room=new Array();
    var name_room=new Array();
    var i=0;
    var j=0;
    $('.hidden').each(function() {
      num_room[i]=this.value;
      i++;
    });
    //console.log(num_room.length);
    $('.hidden_name_room').each(function() {
      name_room[j]=this.value;
      j++;
    });

    var hidden_type=$(".hidden_type").val();
    //ajax
    var href=window.location.hostname;
    $.get('../../../../ajax-id?id_type='+ id,function(data) { //truyen id_type qua cho route
    a=0;//gán cờ để kiểm tra 
    if (id==hidden_type) {
      var i=0;
      var string='';
      while(i< num_room.length) {
        string+= "<input type='checkbox' name='num_room[]' value="+num_room[i] + " checked >" + name_room[i] + " " ;
        $('#num_room').html(string);
        i++;
      }
      a=1;
    }
    else {
       $('#num_room').html("Không còn phòng trống!");
    }
    if(data.length !=0) {
      //alert();
      var string='';
      var i=0;
      $.each(data,function(index,subOb) {    
        string+="<input type='checkbox' name='num_room[]' value="+subOb.id +">" +subOb.name+" ";
        // if(i < num_room.length) {
        //   if(subOb.id==num_room[i])
        //     string+=" checked";
        // }
        //string+=">" +subOb.name+" "; 
        if (id==hidden_type && a==1) {
          string1="<input type='checkbox' name='num_room[]' value="+subOb.id +">" +subOb.name+" ";
          $('#num_room').append(string1);
        }
        else {
          $('#num_room').html(string);
        }
       // i++;
      });         
    }  
     $("input[type='checkbox']").change(function() {
        var checkbox=$("input[type='checkbox']");
        validation($('#error_numroom'),checkbox.is(':checked'),"Chọn phòng");
      }); 
      // else $('#num_room').html("Không còn phòng trống!");
    });
  });
  $("#date_go").change(function() {
    // var date_from;
    // var date_go;
    //$date_from= date("Y-m-d",strtotime($data['date_from']))
    var date_from=$('#date_from').val();
    var date_go=$('#date_go').val(); 
    var date_from_cv =  parseDate(date_from);
    var date_go_cv =  parseDate(date_go);
    if(date_go && date_from) {
      validation($('#error_date'),date_from_cv <= date_go_cv,"Ngày đến phải nhỏ hơn ngày đi");
    }
  });
  $("#child").change(function() {    
    var child=$("#child").val();
    var people=$("#people").val();
    validation($("#error_child"),child>0 || people>0,"Số lượng ít nhất là 1"); 
  });
  $("#people").change(function() { 
     $("#child").change();
  });
});

//function validate
error=new Array();
function validation(varible,condition=true, message='') {
  if(!condition) {
    varible.css('background','url(../../../images/delete.png) no-repeat left');
    varible.html(message);
    $(".btn-book").prop('disabled',true);//disabled button
    var index=error.indexOf(message);//find index of error
    if(index==-1) { //if don't find message in array error, insert message in array error
      error.push(message);
    }
  }
  else {
    var index=error.indexOf(message)
    if(index!=-1) {
      error.splice(index,1);//delete message in array error
    }
    varible.css('background','url(../../../images/accept.png) no-repeat left');
    varible.html('OK');
   check_empty();   
  }
}

function check_empty() {
  if(error.length>0) //check if have error return disable button 
    return $(".btn-book").prop('disabled',true); 
  var data= [ $('#room_type'),
              $('#date_go'),
              $('#date_from'),
              $('#name'),
              $('#gender'), 
              $('#address'), 
              $('#city'),   
              $('#email'),
              $('#phone'), 
            ];
  var id= ($("input[type='checkbox']").is(':checked'))? 1:0;
  var i;
  for(i=0;i<data.length;i++) {
    if(!data[i].val() || id==0) {
      return $(".btn-book").prop('disabled',true); 
    }
  } 
  return $(".btn-book").prop('disabled',false);
}

//function convert dd/mm/yy to (y,m,d)
function parseDate(str) {
  var mdy = str.split('-');
  var a=mdy[2] + "," + mdy[1] + "," + mdy[0];
  return new Date(a);
}
function error_empty(varible) {
  //varible.css('background','url(../images/delete.png) no-repeat left');
  varible.html("Không được trống");
}

$("document").ready(function() { 
  var href=window.location.pathname;
  $("#room_type").change();
  if(href=='/users/booking') {
    $('#date_go').change();
    $("#child").change();
  }
   //console.log()

  $(".btn-book").prop('disabled',true); //default  btn-book disable true
  $("#room_type").change(function() {
    var room_type=$("#room_type").val();
    validation($('#error_roomtype'),room_type.length!=0,"Chọn loại phòng");
    $('#error_numroom').css('background','none');
    $('#error_numroom').html('');
  });
  $("#date_from").change(function() {
    $("#date_go").change();
  }); 
  $("#name").change(function() {
    var name=$("#name").val();
  //  var match="^[a-zA-Z\\s]*$";
    var match="([a-zA-Z]{2,30}\s*)";
    validation($('#error_name'),name.match(match),"Họ và tên không đúng");
    if(!name) {
      error_empty($('#error_name'));
    }
  });
  $("#gender").change(function() {
    validation($('#error_gender'));
  });
  $("#address").change(function() {
    validation($('#error_address'));
    if(!$("#address").val()) {
      error_empty($('#error_address'));
    }
  });
  $("#city").change(function() {
    validation($('#error_city'));
    if(!$("#city").val()) {
      error_empty($('#error_city'));
    }
  });
  $("#country").change(function() {
    validation($('#error_country'));
  });
  $("#email").change(function() {
    var email=$("#email").val();
    
    $condition=email.match(/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/);
    if(!$condition) {
      $("#error_email").css('background','url(../images/delete.png) no-repeat left');
      $("#error_email").html("Email không đúng");
      $(".btn-book").prop('disabled',true);//disabled button 
      var index=error.indexOf("Email không đúng");
      if(index==-1) 
      error.push("Email không đúng"); 
    }
    else {
      var index=error.indexOf("Email không đúng");
      if(index!=-1) 
      error.splice(index,1);
      $.get('../../../../ajax-email?email='+ email,function(data) { //truyen id_type qua cho route
      validation($('#error_email'),data.length==0,"Email đã tồn tại");
      });
    }   
    if(!$("#email").val()) {
      error_empty($('#error_email'));
    }
  });
  $("#phone").change(function() {
    var phone=$("#phone").val();
    validation($('#error_phone'),phone.match(/^\d{10,11}$/),"Số điện thoại không đúng");
    if(!$("#phone").val()) {
      error_empty($('#error_phone'));
    }
  });
});
$("document").ready(function(){
  $("div.alert").delay(3000).slideUp(500);
});