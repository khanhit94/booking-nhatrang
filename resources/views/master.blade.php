<?php
  use Illuminate\Support\Facades\Auth;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equive="X-UA-Compatible" content="IE=edge">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Nha Trang</title>
	<link rel="stylesheet" type="text/css" href="{!! url('css/bootstrap-theme.min.css') !!}">
	<link rel="stylesheet" type="text/css" href="{!! url('css/bootstrap.min.css') !!}">
  <link rel="stylesheet" type="text/css" href="{!! url('css/style_fix.css') !!}">
  <link rel="stylesheet" type="text/css" href="{!! url('css/datepicker.css') !!}">
  @yield('head')
</head>
<body>
  <div id="header">
    <div class="top-menu">
      <div class="container">
		    <div class="row">
				  <div class="col-md-6 col-sm-6 col-xs-2 "></div>
				  <div class="col-md-3 col-sm-3 col-xs-3">
					  <div id="search">
						<form>
							<input type="text" name="search" placeholder="SEARCH">
						</form>
					  </div>
				  </div>
				  <div class="col-md-3 col-sm-3 col-xs-7">
					  <div id="language">
						  <select id="english">
							  <option>ENGLISH</option>
						  </select>
					  </div>
				  </div>
			  </div>
		  </div>
	  </div>
    <nav class="navbar navbar-inverse navbar-static-top">
		  <div class="container">
    	  <div class="navbar-header">
        	<div class="container">
        		<div class="row">
        			<div class="col-xs-2">
              	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                	<span class="sr-only">Toggle navigation</span>
                	<span class="icon-bar"></span>
                	<span class="icon-bar"></span>
                	<span class="icon-bar"></span>
              	</button>
              </div>
        			<div class="col-xs-2"></div>
         			<div class="col-xs-4">
         				<div id="sm-logo">              
                  <img src="{!! url('images/sm-icon.png') !!}" class="navbar-brand navbar-right">
                </div>
              </div>
              <div class="col-xs-4">
              	<div class="btn btn-primary booking1">Booking
              	</div>
              </div>       
            </div>
          </div> 
        </div><!--end .navbar-header-->
		    <div class="navbar-collapse collapse" id="menu">
          <div id="img1"><a href="{!! url('/') !!}"><img src="{!! url('/images/logo.png') !!}"></a></div>
          <ul class="nav navbar-nav">
            <li><a href="{!! url('/') !!}">Trang chủ</a></li>
            <li><a href="">Giới thiệu</a></li>
            <li><a href="">Loại phòng</a></li>
            <li><a href="">Dịch vụ</a></li>
            <li><a href="">Nhà hàng-Bar</a></li>
            <li class="hiden1"><a href="">Hình ảnh</a></li>
            <li class="hiden1"><a href="">Tin tức</a></li>
            <li class="hiden"><a href="">Tuyển dụng</a></li>
            @if (Auth::check())
            <?php $id_customer = Auth::id();?>
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="glyphicon glyphicon-user">&nbsp</span>{{ Auth::user()->name }}
                <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li>
                    <a href="<?php echo url('/users/booking').'/'.$id_customer?>">
                      <span class="glyphicon glyphicon-info-sign"></span> Thông tin phòng
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo url('/users/booking').'/'.$id_customer.'/edit' ?>">
                      <span class="glyphicon glyphicon-info-sign"></span> Chỉnh sửa phòng
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/logout') }}">
                      <span class="glyphicon glyphicon-log-out"></span> Logout
                    </a>
                  </li>
                </ul>
              </li>
            @else 
              <li><a href="{{ url('login') }}""><span class="glyphicon glyphicon-user">&nbsp</span>Login</a></li>
            @endif
            
          </ul>
        </div>
      </div><!-- end .container-->
 	  </nav>
 	  <div class="content">
 		  <div class="container-fluid">
      <!-- source -->
 			  @yield('content')
        <div id="booking">
          <h3>Đặt phòng:</h3>
          <form method="post" action="{!! url('users/booking')!!}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="date datepicker go type" data-date-format="dd-mm-yyyy"  id="" >
              <input type="text" readonly="" class="input-group-addon" placeholder = "Ngày đến" name="date_from" >
            </div>
            <div class="date datepicker go type" data-date-format="dd-mm-yyyy"  id="" >
              <input type="text" readonly="" class="input-group-addon" placeholder = "Ngày đi" name="date_go">
            </div>
            <div class="go">
              <select name="people">
                <option value="">Người lớn</option>
                @for($i=0;$i<10;$i++)
                  <option value="{!!$i!!}">{!!$i!!}</option>
                @endfor
              </select>
            </div>
            <div class="go">
              <select name="child" >
                 <option value="">Trẻ em</option>
                  @for($i=0;$i<10;$i++)
                  <option value="{!!$i!!}">{!!$i!!}</option>
                @endfor
              </select>
            </div>  
            <div class="btn-booking">
              <input type="submit" name="booking" value="ĐẶT PHÒNG" style="background:#12b4e9;width: 135px;height: 50px;margin:21px 81px;"">
            </div>
          </form>
        </div>
        <div class="footer">
 				  <div class="row">
            <div class="col-md-1 col-sm-1"></div>
 					  <div class="col-md-2 col-xs-4 col-sm-12 hotel">
 						  <div class="sub-title">
                <a href=""><h3>Hotel</h3></a>
                <div class="submenu">
       						<ul class="ht-menu">
       							<li><a href="">Booking</a></li>
       							<li><a href="">About Us</a></li>
       							<li><a href="">Location</a></li>
       							<li><a href="">Contact</a></li>
       						</ul>
                </div>
              </div>
 					  </div>
 					  <div class="col-md-2 col-xs-5 col-sm-12 hotel">
              <div class="sub-title">
 						    <a href=""><h3>Accommodation</h3></a>
                <div class="submenu">
       						<ul>
       							<li><a href="">Room</a></li>
       							<li><a href="">Equipment</a></li>
       							<li><a href="">Services</a></li>
       							<li><a href="">Safety</a></li>
       						</ul>
                </div>
              </div>
 					  </div>
 					  <div class="col-md-2 col-xs-3 col-sm-12 hotel">
              <div class="sub-title">
 						    <a href=""><h3>Services</h3></a>
                  <div class="submenu">
         						<ul>
         							<li><a href="">Accommodation</a></li>
         							<li><a href="">Conferences</a></li>
         							<li><a href="">Gastronomy</a></li>
         							<li><a href="">Relaxation</a></li>
         						</ul>
                  </div>
              </div>
 					  </div>
 					  <div class="col-md-2 col-xs-4 col-sm-12 hotel">
              <div class="sub-title">
 						    <a href=""><h3>Menu</h3></a>
                <div class="submenu">
       						<ul>
       							<li><a href="">Menu restautant Promient</a></li>
       							<li><a href="">Bussiness menu</a></li>
       							<li><a href="">Lopby bar menu</a></li>
       						</ul>
                </div>
              </div>
 					  </div>
 					  <div class="col-md-2 col-xs-8 col-sm-12 hotel">
              <div class="sub-title">
 						    <a href=""><h3>Contact us</h3></a>
                <div class="submenu">
       						<ul>
       							<li><a href="">15B Nguyễn Thiện Thuật, Nha Trang</a></li>
       							<li><a href="">Tell: +84.510.713176</a></li>
       							<li><a href="">Fax: +84.510.33333333</a></li>
       							<li><a href="">Location Google map</a></li>
       						</ul>
                </div>
              </div>
 					  </div>
 					  <div class="col-md-1 col-xs-0 col-sm-1"></div>
 					</div>
 					<div class="copyright">
 					  <div class="row">
 					    <div class="col-md-1 col-xs-1 col-sm-1"></div>
					    <div class="col-md-4 col-xs-12 col-sm-12">
						    <span>Copyright &copy 2013 Vitour-All Right Reseved </span>
					    </div>
 						  <div class="col-md-4 col-xs-12 col-sm-12">
   							<div class="paypal">
   								<img src="{!! url('images/paypal.png') !!}">
   								<img src="{!! url('images/visa.png') !!}">
   								<img src="{!! url('images/express.png') !!}">
   								<img src="{!! url('images/master-card.png') !!}">
   							</div>
 						  </div>
 						  <div class="col-md-3 col-xs-12 col-sm-12">
 							  <div class="letter">
 								  <p>Leave a Message</p>
 							  </div>
 						  </div>
 						</div>
 				  </div>
 			  </div>	
 	    </div>	
    </div>
 	</div>
	<script type="text/javascript" src="{!! url('js/jquery-3.1.1.min.js') !!}"></script>
	<script type="text/javascript" src="{!! url('js/bootstrap.min.js') !!}"></script>
  <script type="text/javascript" src="{!! url('js/bootstrap-datepicker.js') !!}"></script>
  <script type="text/javascript" src="{!! url('/js/myscrip.js') !!}"></script>
</body>
</html>