@extends('users.master-booking')
@section('head')
  <link rel="stylesheet" type="text/css" href="{!! url('css/booking.css') !!}">
@stop

@section('title','THAY ĐỔI ĐẶT PHÒNG')

@section('form')
  <form enctype="multipart/form-data" method="post" action="<?php echo url('/users/booking').'/'.$id_customer ?>" name="booking">
  <input type="hidden" name="_method" value="PUT" id="method">
  <input type="hidden" name="id" value="{!! $id_customer !!}">
@stop
@section('type')
<select class="ip-text room_type" name="room_type" id="room_type">
  <option value="">Chọn loại phòng</option>
  <?php $selected="selected=''" ?>
  @foreach ($type as $key=>$value) 
    <option value= "{!! $value->id !!}"
    @if($value->id==$type_old->id)
      {!!$selected!!}
    @endif
    >{!!$value->name!!}</option> 
  @endforeach  
</select> 
@stop

@section('error_type')
  {!! $errors->first('room_type')!!}
@stop

@section('hidden')
@foreach ($room as $id => $name)
  <input type="hidden" name="" class="hidden" value="{!!$name->id!!}">
  <input type="hidden" name="" class="hidden_name_room" value="{!!$name->name!!}">
 @endforeach 
  <input type="hidden" name="" class="hidden_type" value="{!! $type_old ->id!!}">
@stop

@section('error_room')
  {!! $errors->first('num_room')!!}
@stop

@section('child')
  <?php $selected="selected=''" ?>
  <select class="ip-text" name="child" id="child">
    @for($i=0;$i<10;$i++)
      <option value="{!! $i !!}"
      @if($i==$booking[0]->child)
        {!! $selected !!}
      @endif
      >{!! $i !!}</option>
    @endfor
  </select>
@stop

@section('people')
  <select class="ip-text" name="people" id="people">
    @for($i=0;$i<10;$i++)
      <option value="{!! $i !!}"
      @if($i==$booking[0]->people)
        {!! $selected !!}
      @endif
      >{!! $i !!}</option>
    @endfor
  </select> 
@stop

@section('error_child')
  <?php 
    $child=$errors->first('child');
    $people=$errors->first('people');
    if($child && $people != null) 
      echo"Vui lòng nhập số lượng trẻ em và người lớn";
    else if($child!=null) echo $child;
    else echo $people;
  ?>
@stop

@section('date_from')
<div class=" datepicker date" data-date-format="dd-mm-yyyy" >
  <input type="text"  name="date_from" id='date_from' class="input-group-addon " readonly="" value="{!! $date_from!!}"/>
</div>
@stop

@section('error_date-from')
  {!!$errors->first('date_from')!!}
@stop

@section('date_go')
  <div class=" datepicker date" data-date-format="dd-mm-yyyy">
    <input type="text" id="date_go" name="date_go" class="input-group-addon" readonly="" value="{!! $date_go !!}">
  </div>
@stop

@section('error_date-go')
  {!!$errors->first('date_go')!!}
@stop

@section('note')
  <textarea rows="4" maxlength="100" name="note" style="width: 100%;" value="$booking[0]->note"></textarea>
@stop

@section('name')
  <input type="text" name="name" class="ip-text" id="name" value="{!!$customer->name!!}">
@stop

@section('error_name')
  {!!$errors->first('name')!!}
@stop

@section('gender')
  <select class="ip-text" name="gender" id="gender">
    <option value=1>Nam</option>
    <option value=2
      @if($customer->gender==2)
        {!!"selected"!!}
      @endif
    >Nữ</option>
  </select>
@stop

@section('address')
  <input type="text" name="address" class="ip-text" value="{!!$customer->address!!}" id="address">
@stop

@section('error_address')
  {!!$errors->first('address')!!}
@stop

@section('city')
  <input type="text" name="city" class="ip-text" value="{!!$customer->city!!}" id="city">
@stop

@section('error_city')
  {!!$errors->first('city')!!}
@stop

@section('country')
  <select class="ip-text" name="country" id="country">
    <option value="Việt Nam">Việt Nam</option>
    <option value="Anh"
    @if($customer->country=='Anh')
      {!!"selected"!!}
    @endif
    >Anh</option>
  </select>
@stop

@section('email')
  <input type="text" class="ip-text" name="email" value="{!!$customer->email!!}" id="email"> 
@stop

@section('error_email')
  {!!$errors->first('email')!!}
@stop

@section('phone')
  <input type="text" name="phone" class="ip-text" value="{!!$customer->phone!!}" id="phone">
@stop

@section('error_phone')
  {!!$errors->first('phone')!!}
@stop

@section('button')
  <button type="submit" class="btn btn-default btn-book" name="btn1" onclick="" value="update">Cập nhật</button>
  <button type="submit" class="btn btn-default" name="btn2" onclick="" id="delete" value="delete">Hủy đặt phòng</button> 
@stop

