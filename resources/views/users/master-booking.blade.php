@extends('master')
@section('content')
<div class="container">
  <div class="main-booking">
    @yield('message')
    <h3 class="txt-book">@yield('title')</h3> 
    <div class="row">
      @yield('form')
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="col-md-6 col-xs-12" >
          <div class="book-info">
            <h3>Booking information</h3>
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Loại phòng:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('type')
              </div> 
              <div class="error" id="error_roomtype">@yield('error_type')</div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" >Phòng số:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8" id="num_room">
              <!-- show name room -->          
              @yield('num_room')
              </div>
              @yield('hidden')
              <div class="error" id="error_numroom">@yield('error_room')</div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="child">Trẻ em:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8">
                <div class="row">
                  <div class="col-md-3 col-xs-3 child">
                    @yield('child')
                    <!-- <input type="text" name="child" id="child" class="ip-text" value="{!!old('child')!!}" > -->
                  </div>  
                  <div class="col-md-9 col-xs-9 people">
                    <div class="row">
                      <div class="col-md-2 col-xs-0"></div>
                      <div class="col-md-4 col-xs-6 ">
                        <label for="child">Người lớn:</label><span>*</span>
                      </div>
                      <div class="col-md-6 col-xs-6 people1">
                       @yield('people')
                      </div>
                    </div>
                  </div>
                </div>  
                <div class="error error2" id="error_child">
                  @yield('error_child')
                </div>
              </div> 
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room ">
                <label for="date_from">Ngày đến:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 gone">
                @yield('date_from')
              </div>
              <div class="error" id="error_date">
                @yield('error_date-from')
              </div>
            </div>
          </div>  
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="date_go">Ngày đi:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 gone">
                @yield('date_go')
              </div>
              <div class="error">
                @yield('error_date-go')
              </div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="date_go">Ghi chú:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('note')
              </div>  
            </div>
          </div>  
        </div>
        <div class="col-md-6 col-xs-12" >
          <div class="book-info">
            <h3>Thông tin cá nhân</h3>
              <div class="row">
                <div class="col-md-3 col-xs-4 room">
                  <label for="room_type" ">Họ và tên:</label><span>*</span>
                </div>
                <div class="col-md-9 col-xs-8 ">
                  @yield('name')
                </div>
                <div class="error" id="error_name">
                  @yield('error_name')
                </div>
              </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Giới tính:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('gender')
              </div>
              <div class="error" id="error_gender"></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Địa chỉ:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('address')
              </div>
              <div class="error" id="error_address">
                @yield('error_address')
              </div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Thành phố:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('city')
              </div>
              <div class="error" id="error_city">
                @yield('error_city')
              </div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Quốc tịch:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('country')
              </div>
              <div class="error" id="error_country"></div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Email:</label><span>*</span>   
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('email')     
              </div>
              <div class="error" id="error_email">
                @yield('error_email')
              </div>
            </div>
          </div>
          <div class="book-info">
            <div class="row">
              <div class="col-md-3 col-xs-4 room">
                <label for="room_type" ">Điện thoại:</label><span>*</span>
              </div>
              <div class="col-md-9 col-xs-8 ">
                @yield('phone')
              </div>
              <div class="error" id="error_phone">
                @yield('error_phone')
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-xs-12 button-book">
            @yield('button')
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@stop
