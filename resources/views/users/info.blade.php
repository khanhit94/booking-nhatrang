@extends('users.master-booking')
@section('head')
  <link rel="stylesheet" type="text/css" href="{!! url('css/booking.css') !!}">
@stop
@section('message')
  @if (Session::has('flash_message'))
  <div class="alert alert-success fade in" id="flash">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    {!! Session::get('flash_message') !!}
  </div>
  @endif
@stop
@section('title','THÔNG TIN ĐẶT PHÒNG')

@section('form')
  <form method="post" action="{!! route('index') !!}" name="booking-info">
@stop
@section('type')
  {!! $type->name !!}
@stop

@section('num_room')
  @foreach($room as $value)
    {!! $value->name !!}
  @endforeach
@stop

@section('child')
  {!! $booking->child !!} 
@stop

@section('people')
  {!! $booking->people !!}
@stop

@section('date_from')
  {!! $booking->date_from !!}
@stop

@section('date_go')
  {!! $booking->date_go !!}
@stop

@section('note')
  @if($booking->note)
    {!! $booking->note !!}
  @else {!! "Không có" !!}
  @endif
@stop

@section('name')
  {!! $customer->name !!}
@stop

@section('gender')
  @if($customer->gender==2)
  {!! "Nữ" !!}
  @else {!! "Nam" !!}
  @endif
@stop

@section('address')
  {!! $customer->address !!}
@stop

@section('city')
  {!! $customer->city !!}
@stop

@section('country')
  {!! $customer->country !!}
@stop

@section('email')
  {!! $customer->email !!}
@stop

@section('phone')
  {!! $customer->phone !!}
@stop

@section('button')
  <button type="submit" class="btn btn-default" name="OK" onclick="">OK</button>
@stop
<style type="text/css">
  .people {
    text-align: left !important;
  }
  span {
    display: none;
  }
  .room label{
    font-size: 14px !important;
  }
</style>
<script type="text/javascript" src="{!! url('js/jquery-3.1.1.min.js') !!}"></script>
<script type="text/javascript">
  $("document").ready(function(){
  $("div.alert").delay(3000).slideUp(500);
  });
</script>

