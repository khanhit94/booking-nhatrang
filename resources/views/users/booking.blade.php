@extends('users.master-booking')
@section('head')
  <link rel="stylesheet" type="text/css" href="{!! url('css/booking.css') !!}">
@stop
@section('title','ĐẶT PHÒNG')

@section('form')
  <form enctype="multipart/form-data" method="post" action="{!! url('users/setbooking') !!}" name="booking">
@stop
@section('type')
<select class="ip-text room_type" name="room_type" id="room_type">
  <option value="">Chọn loại phòng</option>
  <?php $selected="selected=''" ?>
  @foreach ($type as $key=>$value) 
    <option value= "{!! $value->id !!}"
    @if($value->id==old('room_type'))
      {!!$selected!!}
    @endif
    >{!!$value->name!!}</option> 
  @endforeach 
</select>
@stop

@section('error_type')
  {!! $errors->first('room_type')!!}
@stop

@section('num_room')
  @if(old('num_room')!=null)
    @foreach ($room as $id => $name) 
      @foreach (old('num_room') as $key => $value) 
        @if($id==$value)
          <input type='checkbox' name='num_room[]' value='{!!$value!!}' checked >{!!$name!!}
        @endif
      @endforeach
    @endforeach 
    @else {!!"Chọn phòng"!!}
  @endif
@stop

@section('error_room')
  {!! $errors->first('num_room')!!}
@stop

@section('child')
  <select class="ip-text" name="child" id="child">
    @for($i=0;$i<10;$i++)
      <option value="{!! $i !!}"
        @if(isset($child))
          @if($i==$child)
            {!! "selected" !!}
          @endif
        @endif
      >{!! $i !!}</option>
    @endfor
  </select>
@stop

@section('people')
  <select class="ip-text" name="people" id="people">
    @for($i=0;$i<10;$i++)
      <option value="{!! $i !!}"
        @if(isset($people))
          @if($i==$people)
            {!! "selected" !!}
          @endif
        @endif
      >{!! $i !!}</option>
    @endfor
  </select> 
@stop

@section('error_child')
  <?php 
    $child=$errors->first('child');
    $people=$errors->first('people');
    if($child && $people != null) 
      echo"Vui lòng nhập số lượng trẻ em và người lớn";
    else if($child!=null) echo $child;
    else echo $people;
  ?>
@stop

@section('date_from')
<div class=" datepicker date" data-date-format="dd-mm-yyyy" >
  <input type="text"  name="date_from" id='date_from' class="input-group-addon " readonly="" value="{!!isset($date_from)?$date_from:old('date_from')!!}"/>
</div>
@stop

@section('error_date-from')
  {!!$errors->first('date_from')!!}
@stop

@section('date_go')
  <div class=" datepicker date" data-date-format="dd-mm-yyyy">
    <input type="text" id="date_go" name="date_go" class="input-group-addon" readonly="" value="{!!isset($date_go)?$date_go:old('date_go')!!}">
  </div>
@stop

@section('error_date-go')
  {!!$errors->first('date_go')!!}
@stop

@section('note')
  <textarea rows="4" maxlength="100" name="note" style="width: 100%;" value=""></textarea>
@stop

@section('name')
  <input type="text" name="name" class="ip-text" id="name" value="">
@stop

@section('error_name')
  {!!$errors->first('name')!!}
@stop

@section('gender')
  <select class="ip-text" name="gender" id="gender">
    <option value=1>Nam</option>
    <option value=2>Nữ</option>
  </select>
@stop

@section('address')
  <input type="text" name="address" class="ip-text" value="" id="address">
@stop

@section('error_address')
  {!!$errors->first('address')!!}
@stop

@section('city')
  <input type="text" name="city" class="ip-text" value="" id="city">
@stop

@section('error_city')
  {!!$errors->first('city')!!}
@stop

@section('country')
  <select class="ip-text" name="country" id="country">
    <option value="việt nam">Việt Nam</option>
    <option value="anh">Anh</option>
  </select>
@stop

@section('email')
  <input type="text" class="ip-text" name="email" value="" id="email"> 
@stop

@section('error_email')
  {!!$errors->first('email')!!}
@stop

@section('phone')
  <input type="text" name="phone" class="ip-text" value="" id="phone">
@stop

@section('error_phone')
  {!!$errors->first('phone')!!}
@stop

@section('button')
  <button type="submit" class="btn btn-default btn-book" name="booking1" onclick="">Đặt phòng</button>
  <button type="reset" class="btn btn-default btn-restart">Nhập lại</button> 
@stop
