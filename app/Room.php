<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
  protected $table='room';
  protected $fillable=['id','name','id_type','status'];  

  public $timestamps=false;

  public function type() {
  	return $this->belongsTo('App\Type');
  }
}
