<?php
use Illuminate\Support\Facades\Input;
use App\Room;
use App\Customer;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('index');
});
Route::post('/',['as'=>'index', function () {
    return view('index');
}]);
Route::get('ajax-id',function() {
	$id_type=Input::get('id_type');
	$sub=Room::whereRaw('type_id=? and status=?',[$id_type,0])->get();
	// $sub=Room::where(function($query){
	// 	$query->where('id_type','=',$id_type)->get();
	// });
	return Response::json($sub);
	//return view('users.booking')->with(['sub']);
	});
Route::get('ajax-email',function() {
	$message='';
	$email=Input::get('email');
	$exist=Customer::where('email','=',$email)->get();
	return $exist;
	//return Response::json($exist);
});
Route::group(['prefix'=>'users'],function() {
	Route::resource('booking', 'BookingController');
	Route::resource('setbooking', 'SetbookingController',['only' => [ 'store','destroy']]);

	// Route::post('booking-info',['as'=>'insert','uses'=>'Booking1Controller@insert']);
	// Route::get('booking-info',['as'=>'info','uses'=>'BookingController@info']);
	Route::get('login',function() {
		return view('auth.login');
	});

	// Route::resource('booking', 'Booking1Controller');
	Route::post('login',['as'=>'login','uses'=>'LoginController@login']);
	Route::get('delete',['as'=>'delete','uses'=>'BookingController@delete']);
	Route::post('delete',['as'=>'delete','uses'=>'BookingController@delete']);
});
Route::auth();
Route::get('/home', 'HomeController@index');
Route::controllers([
	'auth'=>'Auth\AuthController',
	'password'=>'Auth\PasswordController'

]
);
