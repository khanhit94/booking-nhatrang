<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Http\Controllers\Controller;

use Response;
use App\Http\Requests\BookingRequest;
use App\Customer;

class NhatrangController extends Controller
{
    public function index() {
	    $customer = \DB::table('customer')
	           ->join('booking', 'customer.id', '=', 'booking.id_customer')
	            ->get();
	    return Response::json([
	         'data' => $customer
	    ], 200);
	}
	public function show($id){
	    $customer = \DB::table('customer')
	           ->join('booking', 'customer.id', '=', 'booking.id_customer')
	           ->where('customer.id','=',$id)
	            ->get();
	    if(!$customer){
	        return Response::json([
	            'error' => [
	                'message' => 'Customer does not exist'
	            ]
	        ], 404);
	    }
	  
	    return Response::json([
	            'data' => $customer
	    ], 200);
	}
	public function store(BookingRequest $request) {
		$joke = Customer::create($request->all());
	  
	    return Response::json([
	       'message' => 'Joke Created Succesfully',
	        'data' => $joke
	    ]);
	}
	public function update(BookingRequest $request, $id) {    
	         
	    $joke = Joke::find($id);
	    $joke->body = $request->body;
	    $joke->user_id = $request->user_id;
	    $joke->save();
	  
	    return Response::json([
	        'message' => 'Joke Updated Succesfully'
	    ]);
	}
}
