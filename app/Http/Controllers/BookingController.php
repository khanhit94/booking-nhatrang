<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Type;
use App\Room;
use App\Booking;
use App\Customer;
use App\Http\Requests\BookingRequest;
use Mail,Hash,Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class BookingController extends Controller
{
  public function index() {
    $type=Type::all();
    //$room=Room::all();
    $room = Room::lists('name','id');
    return view('users.booking',compact('type','room'));
  }
  public function store(Request $request) {
    $type=Type::all();
    $room = Room::lists('name','id');
    $date_from= $request->date_from;
    $date_go= $request->date_go;
    $child= $request->child;
    $people= $request->people;
    //$id=Input::get('date_from');
    return view('users.booking',compact('date_from','date_go','type','room','child','people'));
  }
  public function edit($id) {
    if (Auth::check()) {//check auth logged in
      $id_customer = Auth::id();

      $booking=\DB::table('booking')->where('id_customer', '=', $id)->get();
      $num_room=array();
      foreach ($booking as $key => $value) {
        $num_room[]=$value->id_room;
      }
      $type=Type::all();
      $type_old=Room::findorFail($num_room[0])->type;
      $room=Room::findorFail($num_room);
      $customer=Customer::findorFail($id);

      $date_from= date("d-m-Y",strtotime($booking[0]->date_from));
      $date_go= date("d-m-Y",strtotime($booking[0]->date_go));

      if($id_customer==$id)
        return view('users.edit-booking',compact('id_customer','booking','num_room','type','type_old','room','customer','date_from','date_go'));
      else return redirect()->route('index'); 
    }
    else return redirect()->route('login');
  }
  public function show($id) {
    if (Auth::check()) {//check auth logged in
      $id = Auth::id();
    }
    $booking=Booking::where('id_customer', '=', $id)->get();
    $num_room=array();
    foreach ($booking as $key => $value) {
      $num_room[]=$value->id_room;
    }
    $room=Room::findorFail($num_room);
    $type=Room::findorFail($num_room[0])->type;
    $room_type=$type->id;

    $customer=Customer::findorFail($id);
    $room=Room::findorFail($num_room);
    $type=Type::findorFail($room_type);
    $booking=Booking::where('id_customer', '=', $id)->first();
    return view('users.info',compact('customer','room','type','booking'));
  }
  public function update($id,BookingRequest $request) {
    $id_customer = Auth::id();
    if(Input::has('btn1')) {//check btn1 submit
      $customer=Customer::find($id);
      $customer->name=$request->name;
      $customer->gender=$request->gender;
      $customer->address=$request->address;
      $customer->city=$request->city;
      $customer->country=$request->country;
      $customer->email=$request->email;
      $customer->phone=$request->phone;
      $customer->save();

      $num_room=$request->num_room;
      $date_from= date("Y-m-d",strtotime($request->date_from));//convert data from dd/mm/yy to yy/mm/dd  to save db
      $date_go= date("Y-m-d",strtotime($request->date_go));
      $book = Booking::where('id_customer','=',$id)->lists('id_room');
      if(count($num_room)==count($book)) {
        for($i=0;$i<count($num_room);$i++) {
          Booking::where('id_customer', $id)
          ->where('id_room',$book[$i])
          ->update(['id_room'   => $num_room[$i],
                    'child'     =>$request->child,
                    'people'    =>$request->people,
                    'date_from' =>$date_from,
                    'date_go'   =>$date_go,
                    'note'      =>$request->note
                  ]);
          Room::where('id',$book[$i])->update(['status'=>0]);
          Room::where('id',$num_room[$i])->update(['status'=>1]);
        }
      }
      else if(count($num_room)>count($book)) {
        for($i=0;$i<count($num_room);$i++) {
          if($i<count($book)) {
            Booking::where('id_customer', $id)
          ->where('id_room',$book[$i])
          ->update(
                  [ 'id_room'   =>$num_room[$i],
                    'child'     =>$request->child,
                    'people'    =>$request->people,
                    'date_from' =>$date_from,
                    'date_go'   =>$date_go,
                    'note'      =>$request->note
                  ]);
          // echo $num_room[$i];
          // exit();
          Room::where('id',$book[$i])->update(['status'=>0]);
          Room::where('id',$num_room[$i])->update(['status'=>1]);
          }
          else {
            $booking=new Booking;
            $booking->id_room=$num_room[$i];
            $booking->id_customer=$id;
            $booking->child=$request->child;
            $booking->people=$request->people;
            $booking->date_from=$date_from;
            $booking->date_go=$date_go;
            $booking->note=$request->note;
            $booking->save();

            \DB::table('room')->where('id', $num_room[$i])->update(['status' => 1]);
          }
        }
      }
      else {//count($num_room)<count($book)
        for($i=0;$i<count($book);$i++) {
          if($i<count($num_room)) {
            Booking::where('id_customer', $id)
            ->where('id_room',$book[$i])
            ->update(
                    [ 'id_room'   =>$num_room[$i],
                      'child'     =>$request->child,
                      'people'    =>$request->people,
                      'date_from' =>$date_from,
                      'date_go'   =>$date_go,
                      'note'      =>$request->note
                    ]);
            Room::where('id',$book[$i])->update(['status'=>0]);
            Room::where('id',$num_room[$i])->update(['status'=>1]);     
          }
          else {
            Room::where('id',$book[$i])->update(['status'=>0]);
            Booking::where('id_room', $book[$i])->delete();
          }
        }
      }
      session()->flash('flash_message', 'Cập nhật thành công!!!');
       return redirect()->route('users.booking.show', ['id' => $id]); //url:booking/{id}
    }
     else {//btn delete submit
      $date= date("Y-m-d");//get date now
      $date_from = Booking::where('id_customer','=',$id)->lists('date_from');
      $date_go = Booking::where('id_customer','=',$id_customer)->lists('date_go');
     // // echo $date;
     // // print_r($date_from ) ;
      // if($date_from > $date) echo "co";
     // // else echo "ko";
     // // exit();
       if($date_from[0] > $date ) {
        echo "<script>        
              if (confirm('Bạn có chắc muốn hủy??')) 
              window.location='/users/delete';
              else window.location='/users/booking/$id/edit';
              </script>";
       }
      else {
        echo "<script> alert('Không thể hủy vì đã quá ngày');
              window.location='/users/booking/$id/edit';
              </script>";
      }
    }
  }
  public function delete() {
    if (Auth::check()) {
    $id_customer = Auth::id();
    $get_id_room=\DB::table('booking')->where('id_customer', '=', $id_customer)->select('id_room')->get();
    // echo "<pre>";
    // print_r($get_id_room);
    // echo "</pre>";
    foreach ($get_id_room as $key => $value) {
      Room::where('id',$value->id_room)->update(['status'=>0]);
    }
    //echo $get_id_room[0]->id_room;
    //Room::where('id',$get_id_room[0]->id_room)->update(['status'=>0]);
    //exit();
    Booking::where('id_customer', $id_customer)->delete();
    Customer::find($id_customer)->delete();
    return redirect()->route('index');
    }
    else return redirect()->route('index'); 
  }
}
