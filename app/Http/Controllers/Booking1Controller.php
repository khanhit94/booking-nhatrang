<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Type;
use App\Room;
use App\Booking;
use App\Customer;
use App\Http\Requests\BookingRequest;
use Mail,Hash,Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class Booking1Controller extends Controller
{
  public function index() {
    $type=Type::all();
    //$room=Room::all();
    $room = Room::lists('name','id');
    return view('users.booking',compact('type','room'));
  }
  public function store(Request $request) {
      $type=Type::all();
    //$room=Room::all();
    $room = Room::lists('name','id');
    $date_from= $request->date_from;
    $date_go= $request->date_go;
    $child= $request->child;
    $people= $request->people;
    //$id=Input::get('date_from');
    return view('users.booking',compact('date_from','date_go','type','room','child','people'));
  }
  public function edit($id) {
    if (Auth::check()) {//check auth logged in
      $id_customer = Auth::id();
      if($id_customer==$id)
        return view('users.edit-booking',compact('id_customer'));
      else return redirect()->route('index'); 
    }
    else return redirect()->route('login');
  }
  public function show($id) {
    //if (Auth::check()) {//check auth logged in
      //$id_customer = Auth::id();
      $booking=Booking::where('id_customer', '=', $id)->get();
      $num_room=array();
      foreach ($booking as $key => $value) {
        $num_room[]=$value->id_room;
      }
      $room=Room::findorFail($num_room);
      $type=Room::findorFail($num_room[0])->type;
      $room_type=$type->id;
      return view('users.info',compact('id','num_room','room_type'));
   // }
  }
  public function update($id,BookingRequest $request) {
    $id_customer = Auth::id();
    if(Input::has('btn1')) {//check btn1 submit
      $customer=Customer::find($id);
      $customer->name=$request->name;
      $customer->gender=$request->gender;
      $customer->address=$request->address;
      $customer->city=$request->city;
      $customer->country=$request->country;
      $customer->email=$request->email;
      $customer->phone=$request->phone;
      $customer->save();

      $num_room=$request->num_room;
      $date_from= date("Y-m-d",strtotime($request->date_from));//convert data from dd/mm/yy to yy/mm/dd  to save db
      $date_go= date("Y-m-d",strtotime($request->date_go));
      $book = Booking::where('id_customer','=',$id)->lists('id_room');
      if(count($num_room)==count($book)) {
        for($i=0;$i<count($num_room);$i++) {
          Booking::where('id_customer', $id)
          ->where('id_room',$book[$i])
          ->update(['id_room'   => $num_room[$i],
                    'child'     =>$request->child,
                    'people'    =>$request->people,
                    'date_from' =>$date_from,
                    'date_go'   =>$date_go,
                    'note'      =>$request->note
                  ]);
        }
      }
      else if(count($num_room)>count($book)) {
        for($i=0;$i<count($num_room);$i++) {
          if($i<count($book)) {
            Booking::where('id_customer', $id)
          ->where('id_room',$book[$i])
          ->update(
                  [ 'id_room'   =>$num_room[$i],
                    'child'     =>$request->child,
                    'people'    =>$request->people,
                    'date_from' =>$date_from,
                    'date_go'   =>$date_go,
                    'note'      =>$request->note
                  ]);
          }
          else {
            $booking=new Booking;
            $booking->id_room=$num_room[$i];
            $booking->id_customer=$id;
            $booking->child=$request->child;
            $booking->people=$request->people;
            $booking->date_from=$date_from;
            $booking->date_go=$date_go;
            $booking->note=$request->note;
            $booking->save();
          }
        }
      }
      else {
        for($i=0;$i<count($book);$i++) {
          if($i<count($num_room)) {
            Booking::where('id_customer', $id)
            ->where('id_room',$book[$i])
            ->update(
                    [ 'id_room'   =>$num_room[$i],
                      'child'     =>$request->child,
                      'people'    =>$request->people,
                      'date_from' =>$date_from,
                      'date_go'   =>$date_go,
                      'note'      =>$request->note
                    ]);
          }
          else {
            Booking::where('id_room', $book[$i])->delete();
          }
        }
      }
      session()->flash('flash_message', 'Cập nhật thành công!!!');
       return redirect()->route('users.booking.show', ['id' => $id]); //url:booking/{id}
    }
    else {//btn delete submit
      $date= date("Y-m-d");//get date now
      $date_from = Booking::where('id_customer','=',$id)->lists('date_from');
     // $date_go = Booking::where('id_customer','=',$id_customer)->lists('date_go');
     // echo $date;
     // print_r($date_from ) ;
     // if($date_from > $date) echo "co";
     // else echo "ko";
     // exit();
      // if($date_from[0] > $date ) {
      //   echo "<script>        
      //         if (confirm('Bạn có chắc muốn hủy??')) 
      //         window.location='/users/booking/$id_customer';
      //         else window.location='/users/edit';
      //       })
      //         </script>";
      // }
      // else {
      //   echo "<script> alert('Không thể hủy vì đã quá ngày');
      //         window.location='/users/edit';
      //         </script>";
      // }
    }
  }
  public function destroy($id) {
    //$id_customer = Auth::id();
    Booking::where('id_customer', $id)->delete();
    Customer::find($id)->delete();
    return redirect()->route('index');
  }
}
