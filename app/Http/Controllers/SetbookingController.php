<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Type;
use App\Room;
use App\Customer;
use App\Booking;
use App\Http\Requests\BookingRequest;
use Mail,Hash,Session;


class SetbookingController extends Controller
{
	public function index() {
    $type=Type::all();
    //$room=Room::all();
    $room = Room::lists('name','id');
    return view('users.booking',compact('type','room'));
  }
	public function store(BookingRequest $request) {
    $id=\DB::table('customer')->max('id');//get value id_customer max
  	$customer=new Customer;
  	$customer->name=$request->name;
  	$customer->gender=$request->gender;
  	$customer->address=$request->address;
  	$customer->city=$request->city;
  	$customer->country=$request->country;
  	$customer->email=$request->email;
  	$customer->phone=$request->phone;
    $customer->remember_token=$request->_token;

    $name=explode(" ",$request->name);//get last name
    $username= $name[count($name)-1].($id + 1) ;
    $pass= $name[count($name)-1].rand(10000,10000000);

    $customer->username=$username;
    $customer->password=Hash::make($pass);
    $customer->save();

    $id_customer=$customer->id;//id inserted
    $num_room=$request->num_room;
    $room_type=$request->room_type;
    $date_from= date("Y-m-d",strtotime($request->date_from));//convert data from dd/mm/yy to yy/mm/dd  to save db
    $date_go= date("Y-m-d",strtotime($request->date_go));
    foreach ($num_room as $value) {
      $book = new Booking;
      $book->id_customer=$id_customer;
      $book->id_room=$value;
      $book->child=$request->child;
      $book->people=$request->people;
      $book->date_from=$date_from;
      $book->date_go=$date_go;
      $book->note=$request->note;
      $book->save();

      \DB::table('room')->where('id', $value)->update(['status' => 1]);
  	}
    
    $data=['username'=>$username,'password'=>$pass,'name'=>$request->name];
    $mail=$request->email;
    Mail::send('users.mail',$data,function($msg) use ($mail){
      $msg->from('lephuockhanh94@gmail.com','Nha Trang Hotel');
      $msg->to($mail)->subject('Đặt phòng tại Nha Trang Hotel');
    });
    Session::put('id_customer', $id_customer);
    Session::put('num_room', $num_room);
    Session::put('room_type', $room_type);

    session()->flash('flash_message', 'Đặt phòng thành công!!!Hãy kiểm tra email');
    //return view('users.info',compact('id_customer','num_room','room_type'));
    return redirect()->route('users.booking.show', ['id' => $id_customer]); //url:booking/{id}
  }
}
