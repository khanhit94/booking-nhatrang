<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\Customer;

class JokesController extends Controller
{
	public function __construct(){
    //$this->middleware('auth.basic', ['only' => 'store']);
    $this->middleware('jwt.auth');
}
   public function index(){
    $jokes = Customer::all();
    return Response::json([
         'message' => $jokes
    ], 200);
	}
	public function show($id){
     //$joke = Customer::find($id);
		//$joke = Customer::all();
    //return print_r($joke);

	 $joke = Customer::with(
         array('Booking'=>function($query){
            $query->select(array('child'))->get();
        })
        )->find($id);
  
    if(!$joke){
        return Response::json([
            'error' => [
                'message' => 'Joke does not exist'
            ]
        ], 404);
    }
   //return $joke['booking'][0]['child'];
  	return Response::json([
            'data' => $joke
    ], 200);
    // return Response::json([
    //         'data' => $this->transform($joke)
    // ], 200);
	}

	private function transformCollection($jokes){
    return array_map([$this, 'transform'], $jokes->toArray());
	}
  
	private function transform($joke){
	    return [
	       'joke_id' => $joke['id']

	     ];
	}
}
