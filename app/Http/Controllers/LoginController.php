<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BookingRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	public function edit(){
		if (Auth::check()) {
    	return redirect()->route('booking/edit');
		}
		return redirect()->route('login');
	}
	public function login(Request $request){
	$data=[
				'username'=>$request->email,
				'password'=>$request->password
				];
	if(auth::attempt($data)) {
    return redirect()->route('index');
  }
 else return redirect()->route('login');
	}
}