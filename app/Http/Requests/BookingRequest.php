<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
           'room_type'=>'required',
            'num_room'=>'required',
            'child'=>'required',
            'people'=>'required',
            'date_from'=>'required',
            'date_go'=>'required',
            'name'=>'required',
            'address'=>'required',
            'city'=>'required',
            'email'=>'required',
            'phone'=>'required',
        ];
     }

    public function messages()
    {
        return [
            'room_type.required' => 'Vui lòng chọn loại phòng',
            'num_room.required' => 'Vui lòng chọn tên phòng',
            'child.required'  => 'Vui lòng nhập số lượng trẻ em',
            'people.required'  => 'Vui lòng nhập số lượng người lớn',
            'date_from.required'  => 'Vui lòng chọn ngày đến',
            'date_go.required'  => 'Vui lòng chọn ngày đi',
            'name.required'  => 'Vui lòng nhập tên',
            'address.required'  => 'Vui lòng nhập địa chỉ',
            'city.required'  => 'Vui lòng nhập thành phố',
            'email.required'  => 'Vui lòng nhập email',
            'phone.required'  => 'Vui lòng nhập số điện thoại',
        ];
    }
}
