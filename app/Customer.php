<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $table='customer';
  protected $fillable=['id','name','gender','adress','city','country','email','phone'];  
  public $timestamps=true;

  public function booking(){
    return $this->hasMany('App\Booking');
}

}
