<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  protected $table='booking';
  protected $fillable=['id','id_customer','id_room','child','people','date_from','date_go','note'];  
  public $timestamps=false;

   public function customer(){
    return $this->belongsTo('App\Customer');
}
}

