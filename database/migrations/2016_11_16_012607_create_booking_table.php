<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('booking', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_customer');
      $table->integer('id_room');
      $table->integer('child');
      $table->integer('people');
      $table->date('date_from');
      $table->date('date_go');
      $table->string('note');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('booking');
  }
}
