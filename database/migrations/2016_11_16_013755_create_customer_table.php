<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
  {
    Schema::create('customer', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->integer('gender');
      $table->string('adress');
      $table->string('city');
      $table->string('country');
      $table->string('email');
      $table->integer('phone');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('customer');
  }
}
