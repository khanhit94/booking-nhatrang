-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2016 at 03:58 AM
-- Server version: 5.7.12-0ubuntu1.1
-- PHP Version: 7.0.8-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nhatrang`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `people` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_go` date NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `id_customer`, `id_room`, `child`, `people`, `date_from`, `date_go`, `note`) VALUES
(77, 83, 3, 5, 0, '2016-11-30', '2016-12-09', ''),
(80, 85, 4, 2, 2, '2016-12-07', '2016-12-29', ''),
(81, 86, 4, 3, 0, '2016-12-08', '2016-12-28', ''),
(84, 89, 4, 4, 0, '2016-11-16', '2017-01-18', ''),
(85, 90, 2, 2, 0, '2016-12-06', '2016-12-21', ''),
(86, 92, 5, 4, 2, '2016-12-13', '2016-12-26', ''),
(87, 93, 2, 2, 0, '2016-12-06', '2016-12-27', ''),
(88, 94, 6, 5, 3, '2016-12-06', '2016-12-13', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `gender`, `address`, `city`, `country`, `email`, `phone`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(83, 'khanhtt', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh94@gmail.co', '1647673239', 'khanhtt83', '$2y$10$RmSV5Y2ICSxWLElH8.WPX.0pKhIrWZnvE9qlpaQhfjT7EFIAaSeLG', 'W1kwcZ7zCRwwhvIuQicvpQa2lYhvxeIARRTCHuLd', '2016-11-30 06:44:20', '2016-11-30 06:44:20'),
(85, 'khanh', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh94@gmail.us', '1647673239', 'khanh85', '$2y$10$oHtRCdorhXsOEm23.lDMnOATNi8/XkQJkISr1T3bRAtjKweKCnEUe', 'GUlturqYBne6dPyrHX6L5HhhKA6C43ARNM1hccOF', '2016-12-01 03:45:08', '2016-12-01 03:45:08'),
(86, 'khanhkaka', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh94@yahoo.com', '1647673239', 'khanhkaka86', '$2y$10$7cdOEhzKnTaWq5DDAmhnKOLs5a8qA2zmpMEXsiyvT0LNzM9sA9/pe', 'GUlturqYBne6dPyrHX6L5HhhKA6C43ARNM1hccOF', '2016-12-01 03:53:22', '2016-12-01 03:53:22'),
(89, 'khanh', 1, 'điện bàn', 'quảng nam', 'Việt Nam', 'lephuockhanh94@gmail.com', '1647673239', 'khanh87', '$2y$10$Ob8uMaoA3inEseJiQgUPj.jvWbJqtWvFGfBqPOqtwexrjufp4SWMi', 'Z9tsFKNCJnO2AQAmMrDIs28MmPcUb6Tzu4JAn1H9DPHols5ZCzER5PzHNUMM', '2016-12-02 03:10:41', '2016-12-02 10:13:25'),
(90, 'khanh', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh94@yahoo.com.vn', '1647673239', 'khanh90', '$2y$10$KtiyMG.Np7rUd.wqvnZApeScbi9HZ31YH7Isdxo3EfQtqZCZTIYiG', 'O665MFMENKAKhrMrkHK6ifY0WMn0xFTWySO3HGQ5', '2016-12-02 08:41:30', '2016-12-02 08:41:30'),
(91, 'khanh1', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh4@gmail.com', '1647673239', 'khanh191', '$2y$10$mlmYj7WQpo7oMzFyF2E8DeibDzZuohX8Cj/LoVTImASqlcY3ISXKa', 'O665MFMENKAKhrMrkHK6ifY0WMn0xFTWySO3HGQ5', '2016-12-02 08:46:08', '2016-12-02 08:46:08'),
(92, 'khanh1', 1, 'điện bàn', 'quảng nam', 'Việt Nam', 'lephuockhnh94@gmail.co', '1647673239', 'khanh192', '$2y$10$9R6oiatuY0QehmDzAYIDq.pNCwf/kmi28YV0/bQBGiUFbTgFPpftm', 'O665MFMENKAKhrMrkHK6ifY0WMn0xFTWySO3HGQ5', '2016-12-02 08:57:20', '2016-12-05 03:49:30'),
(93, 'khanh1', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lephuockhanh9@gmail.com', '1647673239', 'khanh193', '$2y$10$hCiV.t6A3ryTYbxeqyrVcuh.RX/vclhrPfuMcpIUAIhlwpV/KdwVm', 'h2NqgRTH5caApMNTUt1DOzWi802qqIRo8v7Rg7sI', '2016-12-05 01:15:15', '2016-12-05 01:15:15'),
(94, 'khanh1', 1, 'điện bàn', 'quảng nam', 'việt nam', 'lepuockhanh94@gmail.co', '1647673239', 'khanh194', '$2y$10$sQ0YLKByO.1CINOEilKgduCFWH4D56J02dTNQ6uPGR6/MnnXKtnhO', '7aNrCvdWejS8Xyj9LKBxnKsUI0r5GEuVwdgkDkf1', '2016-12-05 03:39:35', '2016-12-05 03:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_11_16_012607_create_booking_table', 1),
('2016_11_16_013755_create_customer_table', 2),
('2016_11_16_014222_create_room_table', 3),
('2016_11_16_014524_create_type_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `name`, `type_id`, `status`) VALUES
(1, '1T', 1, 1),
(2, '10V1', 2, 1),
(3, '2T', 1, 1),
(4, '20V2', 3, 0),
(5, '3T', 1, 0),
(6, '11V1', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`) VALUES
(1, 'Thường'),
(2, 'VIP1'),
(3, 'VIP2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
